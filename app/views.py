from django.shortcuts import render, redirect, get_object_or_404

from app.models import Resgate
from app.forms import ResgasteForm

#CRUD
def index(request):
    resgate = Resgate.objects.all()
    form = ResgasteForm(request.POST or None, request.FILES or None)

    dados = {'listResgate': resgate, 'form': form}

    return render(request, 'index.html', dados)

#Create
def resgate_new(request):
	
	if request.method == 'POST':

		form = ResgasteForm(request.POST or None, request.FILES or None)

		if form.is_valid():
			form.save()
			return redirect('index')
	else:
		form = ResgasteForm()
	return render(request, 'index.html', {'form':form})