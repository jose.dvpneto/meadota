from django.db import models

class Resgate(models.Model):
    nome = models.CharField("Seu nome",max_length=120)
    telefone = models.CharField("Seu telefone",max_length=20)
    email = models.CharField("Seu e-mail",max_length=199)
    endereco = models.CharField("Endereço onde o animal se encontra",max_length=300)
    image = models.ImageField("Imagem do PET",upload_to='clientes_photos', null=True, blank=True)
    descricao = models.TextField("Descrição do estado do animal, e ponto de referencia!",max_length=999)
    ANIMALS = (
        ('Gato', 'GATO'),
        ('Cachorro', 'CACHORRO'),
        ('Cavalo', 'CAVALO'),
        ('Cobra', 'COBRA'),
        ('Aves', 'AVES'),
        ('Outros', 'OUTROS'),
    ) 
    animals = models.CharField("Animal", max_length=30, choices=ANIMALS)

    def __str__(self):
        return self.animals + '/ ' + self.nome

class Parceiro(models.Model):
    nome = models.CharField("Nome Empresa",max_length=120)
    telefone = models.CharField("Telefone",max_length=20)
    email = models.CharField("E-mail",max_length=199)
    cnpj = models.CharField("CNPJ",max_length=20)
    endereco = models.CharField("Endereço",max_length=300)

    def __str__(self):
        return self.nome