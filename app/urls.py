from django.urls import path

from app.views import resgate_new, index

urlpatterns = [
    
    path('new', resgate_new, name="resgate_new"),

    path('', index, name="index"),

]
