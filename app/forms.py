from django.forms import ModelForm
from app.models import Resgate



class ResgasteForm(ModelForm):
    class Meta:
        model = Resgate
        fields = ['nome', 'telefone', 'email', 'endereco', 'image',  'descricao', 'animals']

