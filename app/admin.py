from django.contrib import admin
from . import models
from .models import  Resgate, Parceiro

admin.site.register(models.Resgate)
admin.site.register(models.Parceiro)